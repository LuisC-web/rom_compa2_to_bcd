# ROM COMPLEMENTO A 2 A BCD con 8 bits pero es escalable 
Debido a que el taller de microprocesadores se necesita convertir un numero en complemento a 2 en base en vhdl.
Se piensa en una rom de complemento a 2  a BCD usando sistema hexadecimal, realizando el siguiente proceso:

- El formato de salida del BCD corresponde a signo+numero BCD en base 2. Ejemplo: Se tienen 127.
      entonces la salida es "0 0001 0010 0111" que corresponde tanto al signo como magnitud por asi decirlo           
- Para hacer la conversion usamos numero decimales para su simplificancion y por el siguiente patron encontrado:

**Patron**

**Si el numero es positivo**:Si se tiene el hexadecimal 00, este corresponde en BCD al numero 0000 en hexadecimal. Que como por cada bit corresponde a 4 bits en binario o sea "0000 0000 0000 0000 ".Con esto se puede respresentar hasta +127.

Si se tiene 127 que corresponde a 7F en hexadecimal pero al mismo tiempo el bcd corresponde a 0127 en hexadecimal que seria lo mismo que "0001 0010 0111" en BCD. O sea que cada numero complemento a 2 positivo en BCD corresponde al numero en decimal correspondiente. Lo prueba los siguientes datos.


| Complemento a 2   | hexadecimal correspondiente | BCD              | hexadecimal correspondiente en BCD |
| ----------------- | --------------------------- | -----------------| -----------------------------------| 
|    0110 1111      |            6F               |  0001 0001 0001  |            0111                    |  
|    0001 0010      |            0A               |  0000 0010 0000  |            0010                    |
|    0010 1011      |            2B               |  0000 0100 0011  |            0043                    |  
|    0001 0010      |            3C               |  0000 0110 0000  |            0060                    |

Ahora teniendo mas claro que el BCD no es mas que el numero en base 10 con formato hexadecimal entonces se puede decir que los negativos igual. Si se sabe que el complemento a 2 de los negativos va desde 1111 1111 a 1000 0000 comenzado con -1 desde el mas significativo se puede decir que en los negativos el complemento a 2 son el reflejo inverso de los positivos. Se observa el siguiente ejemplo:

Se tiene 1111 1111 siendo la posicion FF en hexadecimal o 255 en decimal se tendra en BCD el siguiente codigo 
0000 0000 0001 o sea que para los negativos el equivalente en formato hexadecimal comienza desde la ultima posicion y termina en 1000 0000 que correspondera a 0001  y 0128.

**¿Y el signo?**  

Pues si se observa el hexadecimal posee un bit mas que seria el 4, este indica el signo debido a que se agrega 1 etos corresponden a 4bits mas lo es 0001 si es 1 (negativos)  o 0000 si son positivos.

**Codigo para generar la ROM**

Primero se cuadra el codigo en VDHL



  ```
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
begin
     process(direccion)
    begin
       case direccion is
       **MEMORIA ROM USANDO WHEN**
       end case;
end RTL;        
```
Como es demasido larga la ROM para hacerla a mano, se crea un codigo en python para simplificar
```
  j=128
  def normalizar(numero):
    if numero<10:
      return f"00{numero}"
    elif numero<100:
        return f"0{numero}"
    else:
      return numero
  for i in range(0, 256, 1):
    if i<=127:
      texto=f'when x"{hex(i)[2:]}" =>salida<=x"0{normalizar(i)}";'
      print(texto)
    else  :
      texto=f'when x"{hex(i)[2:]}" =>salida<=x"1{normalizar(j)}";'
      print(texto)
      j-=1      
```
Simplemente genera el codigo para la ROM donde puede ser de libre eleccion dependiendo de las necesidades,
siendo 256-1 el tamaño de la ROM y j=128 el valor de -0 en signo magnitud aprovechado en complemento a 2. Solo es ejecutar el codigo copiar y pegar  el resultado en la parte donde va. 
 
