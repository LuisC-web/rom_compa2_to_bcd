----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    06:48:55 04/27/2024 
-- Design Name: 
-- Module Name:    ROM_COMA2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity ROM_COMA2 is
    port(
        direccion : in  std_logic_vector(7 downto 0);
        salida    : out std_logic_vector(15 downto 0)
    );
end entity ROM_COMA2;



architecture RTL of ROM_COMA2 is
  
begin
     process(direccion)
    begin
       case direccion is
		
when x"00" =>salida<=x"0000";
when x"01" =>salida<=x"0001";
when x"02" =>salida<=x"0002";
when x"03" =>salida<=x"0003";
when x"04" =>salida<=x"0004";
when x"05" =>salida<=x"0005";
when x"06" =>salida<=x"0006";
when x"07" =>salida<=x"0007";
when x"08" =>salida<=x"0008";
when x"09" =>salida<=x"0009";
when x"0a" =>salida<=x"0010";
when x"0b" =>salida<=x"0011";
when x"0c" =>salida<=x"0012";
when x"0d" =>salida<=x"0013";
when x"0e" =>salida<=x"0014";
when x"0f" =>salida<=x"0015";
when x"10" =>salida<=x"0016";
when x"11" =>salida<=x"0017";
when x"12" =>salida<=x"0018";
when x"13" =>salida<=x"0019";
when x"14" =>salida<=x"0020";
when x"15" =>salida<=x"0021";
when x"16" =>salida<=x"0022";
when x"17" =>salida<=x"0023";
when x"18" =>salida<=x"0024";
when x"19" =>salida<=x"0025";
when x"1a" =>salida<=x"0026";
when x"1b" =>salida<=x"0027";
when x"1c" =>salida<=x"0028";
when x"1d" =>salida<=x"0029";
when x"1e" =>salida<=x"0030";
when x"1f" =>salida<=x"0031";
when x"20" =>salida<=x"0032";
when x"21" =>salida<=x"0033";
when x"22" =>salida<=x"0034";
when x"23" =>salida<=x"0035";
when x"24" =>salida<=x"0036";
when x"25" =>salida<=x"0037";
when x"26" =>salida<=x"0038";
when x"27" =>salida<=x"0039";
when x"28" =>salida<=x"0040";
when x"29" =>salida<=x"0041";
when x"2a" =>salida<=x"0042";
when x"2b" =>salida<=x"0043";
when x"2c" =>salida<=x"0044";
when x"2d" =>salida<=x"0045";
when x"2e" =>salida<=x"0046";
when x"2f" =>salida<=x"0047";
when x"30" =>salida<=x"0048";
when x"31" =>salida<=x"0049";
when x"32" =>salida<=x"0050";
when x"33" =>salida<=x"0051";
when x"34" =>salida<=x"0052";
when x"35" =>salida<=x"0053";
when x"36" =>salida<=x"0054";
when x"37" =>salida<=x"0055";
when x"38" =>salida<=x"0056";
when x"39" =>salida<=x"0057";
when x"3a" =>salida<=x"0058";
when x"3b" =>salida<=x"0059";
when x"3c" =>salida<=x"0060";
when x"3d" =>salida<=x"0061";
when x"3e" =>salida<=x"0062";
when x"3f" =>salida<=x"0063";
when x"40" =>salida<=x"0064";
when x"41" =>salida<=x"0065";
when x"42" =>salida<=x"0066";
when x"43" =>salida<=x"0067";
when x"44" =>salida<=x"0068";
when x"45" =>salida<=x"0069";
when x"46" =>salida<=x"0070";
when x"47" =>salida<=x"0071";
when x"48" =>salida<=x"0072";
when x"49" =>salida<=x"0073";
when x"4a" =>salida<=x"0074";
when x"4b" =>salida<=x"0075";
when x"4c" =>salida<=x"0076";
when x"4d" =>salida<=x"0077";
when x"4e" =>salida<=x"0078";
when x"4f" =>salida<=x"0079";
when x"50" =>salida<=x"0080";
when x"51" =>salida<=x"0081";
when x"52" =>salida<=x"0082";
when x"53" =>salida<=x"0083";
when x"54" =>salida<=x"0084";
when x"55" =>salida<=x"0085";
when x"56" =>salida<=x"0086";
when x"57" =>salida<=x"0087";
when x"58" =>salida<=x"0088";
when x"59" =>salida<=x"0089";
when x"5a" =>salida<=x"0090";
when x"5b" =>salida<=x"0091";
when x"5c" =>salida<=x"0092";
when x"5d" =>salida<=x"0093";
when x"5e" =>salida<=x"0094";
when x"5f" =>salida<=x"0095";
when x"60" =>salida<=x"0096";
when x"61" =>salida<=x"0097";
when x"62" =>salida<=x"0098";
when x"63" =>salida<=x"0099";
when x"64" =>salida<=x"0100";
when x"65" =>salida<=x"0101";
when x"66" =>salida<=x"0102";
when x"67" =>salida<=x"0103";
when x"68" =>salida<=x"0104";
when x"69" =>salida<=x"0105";
when x"6a" =>salida<=x"0106";
when x"6b" =>salida<=x"0107";
when x"6c" =>salida<=x"0108";
when x"6d" =>salida<=x"0109";
when x"6e" =>salida<=x"0110";
when x"6f" =>salida<=x"0111";
when x"70" =>salida<=x"0112";
when x"71" =>salida<=x"0113";
when x"72" =>salida<=x"0114";
when x"73" =>salida<=x"0115";
when x"74" =>salida<=x"0116";
when x"75" =>salida<=x"0117";
when x"76" =>salida<=x"0118";
when x"77" =>salida<=x"0119";
when x"78" =>salida<=x"0120";
when x"79" =>salida<=x"0121";
when x"7a" =>salida<=x"0122";
when x"7b" =>salida<=x"0123";
when x"7c" =>salida<=x"0124";
when x"7d" =>salida<=x"0125";
when x"7e" =>salida<=x"0126";
when x"7f" =>salida<=x"0127";
when x"80" =>salida<=x"1128";
when x"81" =>salida<=x"1127";
when x"82" =>salida<=x"1126";
when x"83" =>salida<=x"1125";
when x"84" =>salida<=x"1124";
when x"85" =>salida<=x"1123";
when x"86" =>salida<=x"1122";
when x"87" =>salida<=x"1121";
when x"88" =>salida<=x"1120";
when x"89" =>salida<=x"1119";
when x"8a" =>salida<=x"1118";
when x"8b" =>salida<=x"1117";
when x"8c" =>salida<=x"1116";
when x"8d" =>salida<=x"1115";
when x"8e" =>salida<=x"1114";
when x"8f" =>salida<=x"1113";
when x"90" =>salida<=x"1112";
when x"91" =>salida<=x"1111";
when x"92" =>salida<=x"1110";
when x"93" =>salida<=x"1109";
when x"94" =>salida<=x"1108";
when x"95" =>salida<=x"1107";
when x"96" =>salida<=x"1106";
when x"97" =>salida<=x"1105";
when x"98" =>salida<=x"1104";
when x"99" =>salida<=x"1103";
when x"9a" =>salida<=x"1102";
when x"9b" =>salida<=x"1101";
when x"9c" =>salida<=x"1100";
when x"9d" =>salida<=x"1099";
when x"9e" =>salida<=x"1098";
when x"9f" =>salida<=x"1097";
when x"a0" =>salida<=x"1096";
when x"a1" =>salida<=x"1095";
when x"a2" =>salida<=x"1094";
when x"a3" =>salida<=x"1093";
when x"a4" =>salida<=x"1092";
when x"a5" =>salida<=x"1091";
when x"a6" =>salida<=x"1090";
when x"a7" =>salida<=x"1089";
when x"a8" =>salida<=x"1088";
when x"a9" =>salida<=x"1087";
when x"aa" =>salida<=x"1086";
when x"ab" =>salida<=x"1085";
when x"ac" =>salida<=x"1084";
when x"ad" =>salida<=x"1083";
when x"ae" =>salida<=x"1082";
when x"af" =>salida<=x"1081";
when x"b0" =>salida<=x"1080";
when x"b1" =>salida<=x"1079";
when x"b2" =>salida<=x"1078";
when x"b3" =>salida<=x"1077";
when x"b4" =>salida<=x"1076";
when x"b5" =>salida<=x"1075";
when x"b6" =>salida<=x"1074";
when x"b7" =>salida<=x"1073";
when x"b8" =>salida<=x"1072";
when x"b9" =>salida<=x"1071";
when x"ba" =>salida<=x"1070";
when x"bb" =>salida<=x"1069";
when x"bc" =>salida<=x"1068";
when x"bd" =>salida<=x"1067";
when x"be" =>salida<=x"1066";
when x"bf" =>salida<=x"1065";
when x"c0" =>salida<=x"1064";
when x"c1" =>salida<=x"1063";
when x"c2" =>salida<=x"1062";
when x"c3" =>salida<=x"1061";
when x"c4" =>salida<=x"1060";
when x"c5" =>salida<=x"1059";
when x"c6" =>salida<=x"1058";
when x"c7" =>salida<=x"1057";
when x"c8" =>salida<=x"1056";
when x"c9" =>salida<=x"1055";
when x"ca" =>salida<=x"1054";
when x"cb" =>salida<=x"1053";
when x"cc" =>salida<=x"1052";
when x"cd" =>salida<=x"1051";
when x"ce" =>salida<=x"1050";
when x"cf" =>salida<=x"1049";
when x"d0" =>salida<=x"1048";
when x"d1" =>salida<=x"1047";
when x"d2" =>salida<=x"1046";
when x"d3" =>salida<=x"1045";
when x"d4" =>salida<=x"1044";
when x"d5" =>salida<=x"1043";
when x"d6" =>salida<=x"1042";
when x"d7" =>salida<=x"1041";
when x"d8" =>salida<=x"1040";
when x"d9" =>salida<=x"1039";
when x"da" =>salida<=x"1038";
when x"db" =>salida<=x"1037";
when x"dc" =>salida<=x"1036";
when x"dd" =>salida<=x"1035";
when x"de" =>salida<=x"1034";
when x"df" =>salida<=x"1033";
when x"e0" =>salida<=x"1032";
when x"e1" =>salida<=x"1031";
when x"e2" =>salida<=x"1030";
when x"e3" =>salida<=x"1029";
when x"e4" =>salida<=x"1028";
when x"e5" =>salida<=x"1027";
when x"e6" =>salida<=x"1026";
when x"e7" =>salida<=x"1025";
when x"e8" =>salida<=x"1024";
when x"e9" =>salida<=x"1023";
when x"ea" =>salida<=x"1022";
when x"eb" =>salida<=x"1021";
when x"ec" =>salida<=x"1020";
when x"ed" =>salida<=x"1019";
when x"ee" =>salida<=x"1018";
when x"ef" =>salida<=x"1017";
when x"f0" =>salida<=x"1016";
when x"f1" =>salida<=x"1015";
when x"f2" =>salida<=x"1014";
when x"f3" =>salida<=x"1013";
when x"f4" =>salida<=x"1012";
when x"f5" =>salida<=x"1011";
when x"f6" =>salida<=x"1010";
when x"f7" =>salida<=x"1009";
when x"f8" =>salida<=x"1008";
when x"f9" =>salida<=x"1007";
when x"fa" =>salida<=x"1006";
when x"fb" =>salida<=x"1005";
when x"fc" =>salida<=x"1004";
when x"fd" =>salida<=x"1003";
when x"fe" =>salida<=x"1002";
when x"ff" =>salida<=x"1001";
WHEN others  => salida  <=x"0000";
		
		 end case;
    end process ;

end  RTL;



